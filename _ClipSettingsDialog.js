﻿if (this.exit) {
    $.write('skipping - getSettings');
    //noinspection JSAnnotator
    return;
}
$.write('running - getSettings');

var self = this;

var w = new Window('dialog', 'Data settings for clip');

w.g1 = w.add ('group');
w.g1.add('statictext', [0,0,120,20], 'Data sample rate');
var sampleRateFld = w.g1.add('edittext', [0,0,25,20], this.dataSampleRate);
w.g1.add('statictext', [0,0,120,20], 'seconds');


w.g2 = w.add ('group');
w.g2.add('statictext', [0,0,120,20], 'Clip start time');
var startH = w.g2.add('edittext', [0,0,25,20], 0);
w.g2.add('statictext', undefined, 'h');
var startM = w.g2.add('edittext', [0,0,25,20], 0);
w.g2.add('statictext', undefined, 'm');
var startS = w.g2.add('edittext', [0,0,35,20], 0);
w.g2.add('statictext', undefined, 's');


var h = makeDisplayTimeUnit(Math.floor(this.clipLength / 3600));
var m = makeDisplayTimeUnit(Math.floor((this.clipLength % 3600) / 60));
var s = makeDisplayTimeUnit(Math.round(((this.clipLength % 3600) % 60) * 100) / 100);

w.g3 = w.add ('group');

w.g3.add('statictext', [0,0,120,20], 'Clip length');
w.g3.add('statictext', undefined, h + ':' + m + ':' + s);

/*
w.g3.add('statictext', [0,0,120,20], 'Clip length', {justify: 'right'});
var lengthH = w.g3.add('edittext', [0,0,25,20], h);
w.g3.add('statictext', undefined, 'h');
var lengthM = w.g3.add('edittext', [0,0,25,20], m);
w.g3.add('statictext', undefined, 'm');
var lengthS = w.g3.add('edittext', [0,0,35,20], s);
w.g3.add('statictext', undefined, 's');
*/


w.g3 = w.add ('group');
var btnOk = w.g3.add('button', undefined, 'OK');
var btnCancel = w.g3.add('button', undefined, 'Cancel');

btnOk.onClick = function() {
    self._setSampleRate(sampleRateFld.text);
    self._setClipStartTime(startH, startM, startS);
    // self._setClipLength(lengthH, lengthM, lengthS);
    w.close();
};

btnCancel.onClick = function() {
    self.exit = true;
    w.close();
};

function makeDisplayTimeUnit(t) {
    if (t < 10) return '0' + t;
    return t;
}

w.show();


