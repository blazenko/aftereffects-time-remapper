/**
 * @orignal:
     * KalmanFilter
     * @class
     * @author Wouter Bulten
     * @see {@link http://github.com/wouterbulten/kalmanjs}
     * @version Version: 1.0.0-beta
     * @copyright Copyright 2015 Wouter Bulten
     * @license GNU LESSER GENERAL PUBLIC LICENSE v3
     * @preserve
 *
 * @oldified
     * for ExtendScript by Karo
 */

function KalmanFilter(R, Q, A, B, C) {

    /**
     * Create 1-dimensional kalman filter
     * @param  {Number} options.R Process noise
     * @param  {Number} options.Q Measurement noise
     * @param  {Number} options.A State vector
     * @param  {Number} options.B Control vector
     * @param  {Number} options.C Measurement vector
     * @return {KalmanFilter}
     */

    this.R = R || 1; // noise power desirable
    this.Q = Q || 1; // noise power estimated

    this.A = A || 1;
    this.C = C || 1;
    this.B = B || 0;
    this.cov = NaN;
    this.x = NaN; // estimated signal without noise

    /**
     * Filter a new value
     * @param  {Number} z Measurement
     * @param  {Number} u Control
     * @return {Number}
     */
    this.filter = function(z, u) {
        if (!u) u = 0;

        if (isNaN(this.x)) {
            this.x = (1 / this.C) * z;
            this.cov = (1 / this.C) * this.Q * (1 / this.C);
        }
        else {

            // Compute prediction
            var predX = (this.A * this.x) + (this.B * u);
            var predCov = ((this.A * this.cov) * this.A) + this.R;

            // Kalman gain
            var K = predCov * this.C * (1 / ((this.C * predCov * this.C) + this.Q));

            // Correction
            this.x = predX + K * (z - (this.C * predX));
            this.cov = predCov - (K * this.C * predCov);
        }

        return this.x;
    };

    /**
     * Return the last filtered measurement
     * @return {Number}
     */
    this.lastMeasurement = function() {
        return this.x;
    };

    /**
     * Set measurement noise Q
     * @param {Number} noise
     */
    this.setMeasurementNoise = function(noise) {
        this.Q = noise;
    };

    /**
     * Set the process noise R
     * @param {Number} noise
     */
    this.setProcessNoise = function(noise) {
        this.R = noise;
    };
}
