﻿//@include "_ArrayMapPolyfill.js";
//@include "_KalmanFilter.js";

function Remapper() {
    /// FLOW CONTROL
    this.exit = false;

    /// OPTIONS
    this.useNoiseFilter = true;
    this.noiseFilterPart1 = 0.1;
    this.noiseFilterPart2 = 0.25;

    if (this.useNoiseFilter) {
        this.kalmanFilter = new KalmanFilter(
            this.noiseFilterPart1,
            this.noiseFilterPart2
        );
    }

    /// PROPERTIES
    this.item = app.project.activeItem;
    if (this.item) this.layer = this.item.layer(1);
    if (this.item) this.clipLength = this.layer.outPoint; //this.item.duration;
    this.dataSampleRate = 2;
    this.clipStartTime = 0;

    this.totalDistanceAll = 0;
    this.totalDistanceClip = 0;
    this.averageSpeedAll = 0;
    this.averageSpeedClip = 0;

    this.dataFirstLine = 0;
    this.dataLastLine = 0;
    this.distances = [];
    this.glitches = [];
    this.glitchTreshold = 2;
    this.checkNextDistForGlitch = true;




    /// PUBLIC METHODS
    this.checkSelected = function() {
        $.write('running - checkSelected');
        if (!this.item || !this.layer) {
            alert("Nema klipa ili nije selektiran");
            this.exit = true;
        }
    };




    this.getSettings = function() {
        //@include "_ClipSettingsDialog.js";
    };




    this.checkTimeValues = function() {
        if (this.exit) {
            $.write('skipping - checkTimeValues');
            return;
        }
        $.write('running - checkTimeValues');

        if (this.clipStartTime % this.dataSampleRate !== 0) {
            if (!confirm('Vrijeme početka klipa ('+this.clipStartTime+' sekundi) nije djeljivo sa '+this.dataSampleRate+' (data sample rate). Time Remap bi mogao biti pogrešan. Jeste li sigurni da želite nastaviti?')) {
                this.exit = true;
                return;
            }
        }

        if (this.clipLength % this.dataSampleRate !== 0) {
            if (!confirm('Trajanje klipa ('+(Math.round(this.clipLength * 100) / 100)+' sekundi) nije djeljivo sa '+this.dataSampleRate+' (data sample rate). Time Remap bi mogao biti pogrešan. Jeste li sigurni da želite nastaviti?')) {
                this.exit = true;
                return;
            }
        }
    };




    this.prepareDataSlice = function() {
        if (this.exit) {
            $.write('skipping - prepareDataSlice');
            return;
        }
        $.write('running - prepareDataSlice');

        var skipLines = Math.ceil(this.clipStartTime / this.dataSampleRate);
        this.dataFirstLine = skipLines + 1;

        var readLines = Math.ceil(this.clipLength / this.dataSampleRate);
        this.dataLastLine = skipLines + readLines + 1;

        $.write('1st data line: ' + this.dataFirstLine + ', last data line: ' + this.dataLastLine);
    };




    this.getData = function() {
        if (this.exit) {
            $.write('skipping - getData');
            return;
        }
        $.write('running - getData');

        var currentLine, prevPoint, currPoint, dist = 0;
        var currLineNum = 1;
        var f = File.openDialog('Choose Data File');

        if (!f) {
            this.exit = true;
            return;
        }

        f.open('e');

        while (!f.eof) {
            currentLine = f.readln();

            if (isNaN(parseInt(currentLine))) {
                continue;
            }

            var cols = currentLine.split (',');
            currPoint = {lat:cols[1], lng:cols[2]};

            if (prevPoint) {
                dist = calcDistance(currPoint.lat, currPoint.lng, prevPoint.lat, prevPoint.lng);
                this.totalDistanceAll += dist;

                if (currLineNum > this.dataFirstLine && currLineNum <= this.dataLastLine) {
                    this.distances.push(dist);
                    this.totalDistanceClip += dist;
                }
            }

            prevPoint = currPoint;
            currLineNum++;
        }

        f.close();

        this.averageSpeedAll = this.totalDistanceAll / (currLineNum - 1);
        this.averageSpeedClip = this.totalDistanceClip / (this.dataLastLine - this.dataFirstLine);
        this.glitches = this.distances.map(this._isGlitch, this);

        // $.write(this.distances);
        // $.write('curr: ' + currLineNum);
        $.write('total distance all: ' + this.totalDistanceAll);
        $.write('total distance clip: ' + this.totalDistanceClip);
        $.write('avg speed all: ' + this.averageSpeedAll);
        $.write('avg speed clip: ' + this.averageSpeedClip);
    };




    this.makeRemap = function() {
        if (this.exit) {
            $.write('skipping - makeRemap');
            return;
        }
        $.write('running - makeRemap');

        var origTime, newTime, currSpeed, ratio;
        var currTime = 0;
        this.layer.timeRemapEnabled = true;
        var timeRemap = this.layer.property("Time Remap");

        for (var i=0; i<this.distances.length; i++) {
            origTime = (i+1) * this.dataSampleRate;
            if (origTime > this.clipLength) origTime = this.clipLength;

            if (this.useNoiseFilter) {
                currSpeed = this.kalmanFilter.filer(this._fixGlich(i));
            }
            else currSpeed = this._fixGlich(i);

            ratio = currSpeed / this.averageSpeedAll;
            newTime = currTime + (this.dataSampleRate * ratio);

            timeRemap.setValueAtTime(newTime, origTime);
            currTime = newTime;
        }

        this.clipLength = this.item.duration = this.layer.outPoint = newTime;
    };



    //// PRIVATE METHODS

    this._setClipStartTime = function(hField, mField, sField) {
        this.clipStartTime = makeTime(hField.text, mField.text, sField.text);
    };




    this._setClipLength = function(hField, mField, sField) {
        this.clipLength = makeTime(hField.text, mField.text, sField.text);
    };




    this._setSampleRate = function(time) {
        this.dataSampleRate = parseFloat(time);
    };




    this._isGlitch = function(val, i) {
        var curr = val;
        var next = this.distances[i+1];
        var prev = this.distances[i-1];

        if (!prev || !next) {
            return false;
        }
        else if ((curr < prev && curr < next) || (curr > prev && curr > next)) {
            var diffPrev = Math.abs(curr - prev);
            var diffNext = Math.abs(curr - next);
            return diffPrev > this.glitchTreshold && diffNext > this.glitchTreshold;
        }
        else {
            return false;
        }
    };




    this._fixGlich = function(i) {
        var curr = this.distances[i];

        if (!this.glitches[i]) {
            return curr;
        }
        else if (this.checkNextDistForGlitch && this.glitches[i+1]) {
            return curr;
        }
        else {
            var next = this.distances[i+1];
            var prev = this.distances[i-1];
            return (prev + next) / 2;
        }
    };
}





//////////// UTIL

function makeTime(h, m, s) {
    var time = parseInt(h) * 3600;
    time += parseInt(m) * 60;
    time += parseFloat(s);
    return time;
}

function toRadians(degrees) {
    var pi = Math.PI;
    return degrees * (pi/180);
}

function calcDistance(lat1, lon1, lat2, lon2) {
    //// http://www.movable-type.co.uk/scripts/latlong.html
    var R = 6371e3; // radijus Zemlje
    var φ1 = toRadians(lat1);
    var φ2 = toRadians(lat2);
    var Δφ = toRadians(lat2-lat1);
    var Δλ = toRadians(lon2-lon1);
    var d = Math.acos( Math.sin(φ1)*Math.sin(φ2) + Math.cos(φ1)*Math.cos(φ2) * Math.cos(Δλ) ) * R;
    return d;
}
